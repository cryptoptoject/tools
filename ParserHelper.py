import datetime as DateTime
import pandas as pd


def create_time_and_date_key(date):
    # create key
    # key = long(date_key) + float(time_key) / 1e6
    date = pd.to_datetime(date)
    key = excel_date(date)

    return key


def excel_date(date):
    temp = DateTime.datetime(1899, 12, 30)
    delta = date - temp
    return float(delta.days) + (float(delta.seconds) / 86400)


def get_price_by_time(new_message):
    split_message = new_message.split("ask = ")
    new_ask_price = split_message[1].split(",")

    return new_ask_price[0]


def create_time_key(time):
    # time format hh:mm:ss
    time_hr = int(time[0:2])
    time_min = int(time[3:5])
    time_sec = int(time[6:8])

    time_key = int((time_hr * 1e4) + (time_min * 1e2) + time_sec)

    return time_key


def create_date_key(date):
    # date format yyyy-mm-dd
    date_year = int(date[2:4])
    date_month = int(date[5:7])
    date_day = int(date[8:10])

    date_key = int((date_year * 1e4) + (date_month * 1e2) + date_day)

    return date_key


def parse_oder_message(message):
    # order format - Buy, price = 0.000014, quantity = 161.932392, order number = 2
    tmp_message = message.split(" - ")
    order_data = tmp_message[1]
    operation = order_data.split(",")[0]
    price = (order_data.split("price = ")[1]).split(",")[0]
    quantity = (order_data.split("quantity = ")[1]).split(",")[0]
    order_num = (order_data.split("order number = ")[1]).split(",")[0]

    return [operation, price, quantity, order_num]
