from ParserHelper import *
import csv
import os

# Global variables
SECONDS = 60
NUM_OF_MINUTES_IN_FRAME = 30

TIME_AND_DATE_POS = 0  # position in log line
LOG_LEVEL_POS = 1  # position in log line
MARKET_POS = 2  # position in log line
MESSAGE_POS = 3  # position in log line


# Methods
def create_market_prices_file(file_to_analyze, output_dir):

    # variables
    need_to_set_output_file_name = True
    need_to_set_time_interval = True
    prev_time_key = 0
    time_to_write = 0

    # run on all log file lines and parse them
    for line in file_to_analyze:

        # time_to_write is initialized below - so i don't want to enter this condition until
        # it is initialized
        if time_to_write != 0 and timer != time_to_write:
            # if time is not equal to time_to_write skip this line
            timer += 1
            continue

        data = line.split("|")

        # time and date split
        time_and_date_excel = data[TIME_AND_DATE_POS].strip()
        time_and_date = data[TIME_AND_DATE_POS].split(" ")
        date = time_and_date[0]
        time = time_and_date[1].split(",")
        time = time[0]

        date_time_key = create_time_and_date_key(time_and_date_excel)

        # log level
        log_level = data[LOG_LEVEL_POS].strip()

        # market
        market = data[MARKET_POS].strip()

        # message
        message = data[MESSAGE_POS].strip()

        # set output file name at the first for iteration
        if need_to_set_output_file_name:
            need_to_set_output_file_name = False

            output_dir += "\\" + market + "_" + date
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)

            output_file_name = output_dir + "\\" + market + "_" + date + "_price.csv"
            output_file = open(output_file_name, 'wb')

            with output_file:
                row = ["date_time", "ask_price"]
                writer = csv.writer(output_file)
                writer.writerow(row)

        # calc the logger time interval
        if need_to_set_time_interval:
            time_key = create_time_key(time)
            if prev_time_key == 0:
                prev_time_key = time_key
            else:
                if time_key != prev_time_key:
                    need_to_set_time_interval = False

                    time_interval = time_key - prev_time_key
                    time_frame = NUM_OF_MINUTES_IN_FRAME * SECONDS
                    time_to_write = time_frame / time_interval
                    timer = time_to_write

        # get ask price and write to csv file
        if "ask =" in message and "SL" not in message\
                and not need_to_set_output_file_name\
                and not need_to_set_time_interval:

            timer = 0
            ask_price = get_price_by_time(message)

            # write to csv file
            output_file = open(output_file_name, 'ab')
            with output_file:
                row = [date_time_key, ask_price]
                writer = csv.writer(output_file)
                writer.writerow(row)


def create_market_operations_file(file_to_analyze, output_dir):

    # variables
    buy_order_list = []
    sell_order_list = []
    need_to_set_output_file_name = True

    # run on all log file lines and parse them
    for line in file_to_analyze:

        data = line.split("|")

        # time and date split
        time_and_date_excel = data[TIME_AND_DATE_POS].strip()
        time_and_date = data[TIME_AND_DATE_POS].split(" ")
        date = time_and_date[0]
        time = time_and_date[1].split(",")
        time = time[0]

        date_time_key = create_time_and_date_key(time_and_date_excel)

        # log level
        log_level = data[LOG_LEVEL_POS].strip()

        # market
        market = data[MARKET_POS].strip()

        # message
        message = data[MESSAGE_POS].strip()

        # set output file name at the first for iteration
        if need_to_set_output_file_name:
            need_to_set_output_file_name = False

            output_dir += "\\" + market + "_" + date
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)

            output_file_name = output_dir + "\\" + market + "_" + date + "_operations.csv"
            output_file = open(output_file_name, 'wb')

            with output_file:
                writer = csv.writer(output_file)
                row = ["All orders"]
                writer.writerow(row)
                row = ["date_time", "operation", "price", "quantity", "order number"]
                writer.writerow(row)

        # get ask price and write to csv file
        if "__send_order()" in message and not need_to_set_output_file_name:

            [operation, price, quantity, order_num] = parse_oder_message(message)

            if operation == 'Buy':
                buy_order_list.append([date_time_key, operation, price, quantity, order_num])
            else:  # operation == 'Sell'
                sell_order_list.append([date_time_key, operation, price, quantity, order_num])

            # write to csv file
            output_file = open(output_file_name, 'ab')
            with output_file:
                row = [date_time_key, operation, price, quantity, order_num]
                writer = csv.writer(output_file)
                writer.writerow(row)

    # write only buy orders
    output_file = open(output_file_name, 'ab')
    with output_file:
        writer = csv.writer(output_file)
        row = [""]
        writer.writerow(row)
        row = ["Buy orders"]
        writer.writerow(row)
        row = ["date_time", "operation", "price", "quantity", "order number"]
        writer.writerow(row)

        for order in buy_order_list:
            writer.writerow(order)

    # write only sell orders
    output_file = open(output_file_name, 'ab')
    with output_file:
        writer = csv.writer(output_file)
        row = [""]
        writer.writerow(row)
        row = ["Sell orders"]
        writer.writerow(row)
        row = ["date_time", "operation", "price", "quantity", "order number"]
        writer.writerow(row)

        for order in sell_order_list:
            writer.writerow(order)