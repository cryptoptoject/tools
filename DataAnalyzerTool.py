import ConfigParser
from LogParser import *
import os
import sys

# read config params
# sections
SECTION_DATA_ANALYZER_TOOL = "DataAnalyzerTool"

# options
OPTION_INPUT_DIRECTORY = "input directory"
OPTION_OUTPUT_DIRECTORY = "output directory"
OPTION_CREATE_PRICE_FILE = "create price file"
OPTION_CREATE_OPERATIONS_FILE = "create operations file"

Config = ConfigParser.ConfigParser()
config_file_name = "config.ini"
Config.read(config_file_name)

input_directory = Config.get(SECTION_DATA_ANALYZER_TOOL, OPTION_INPUT_DIRECTORY)
output_directory = Config.get(SECTION_DATA_ANALYZER_TOOL, OPTION_OUTPUT_DIRECTORY)
create_prices_file = bool(int(Config.get(SECTION_DATA_ANALYZER_TOOL, OPTION_CREATE_PRICE_FILE)))
create_operations_file = bool(int(Config.get(SECTION_DATA_ANALYZER_TOOL, OPTION_CREATE_OPERATIONS_FILE)))

dirs = os.listdir(input_directory)
print "converting %d folders" % len(dirs)

# run on all sub folders
for dir_name in dirs:
    dir_full_path = input_directory + "\\" + dir_name

    # analyze files by demand
    for filename in os.listdir(dir_full_path):

        file_full_name = dir_full_path + "\\" + filename

        # create prices file
        if create_prices_file:
            file_to_analyze = open(file_full_name, "r")
            create_market_prices_file(file_to_analyze, output_directory)
            file_to_analyze.close()
            print "finished creating price file from " + file_full_name

        # create operations file
        if create_operations_file:
            file_to_analyze = open(file_full_name, "r")
            create_market_operations_file(file_to_analyze, output_directory)
            file_to_analyze.close()
            print "finished creating operations file from " + file_full_name


print "\nfinished all"
